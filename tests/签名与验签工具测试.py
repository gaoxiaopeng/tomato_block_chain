import binascii

from flaskr.区块链账户 import 区块链账户类
from flaskr.签名与验签工具 import 签名与验签工具

if __name__ == '__main__':
    区块链账户 = 区块链账户类()
    要签名的数据 = '全体目光向我看齐'
    签名 = 签名与验签工具.签名(要签名的数据.encode(), 区块链账户.获取账户私钥())
    验签结果 = 签名与验签工具.验签(要签名的数据.encode(), 签名, 区块链账户.获取账户公钥())
    print('签名:', end='')
    print(binascii.hexlify(签名).decode())
    print('验签结果:', end='')
    print(验签结果)
