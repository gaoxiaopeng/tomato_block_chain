import hashlib
import random

import base58
import ecdsa
from Crypto.Hash import RIPEMD160  # pycryptodome库 用于ripemd-160


class 区块链账户类(object):
    def __init__(self):
        self.私钥 = self.生成私钥()
        self.公钥 = self.生成公钥(self.私钥)
        self.地址 = self.生成地址(self.公钥)

    @staticmethod
    def 生成私钥():
        作为种子的随机数 = ''.join(random.sample('abcdefghijklmnopqrstuvwxyz!@#$%^&*()', 32)).encode()
        return ecdsa.SigningKey.from_string(作为种子的随机数, curve=ecdsa.SECP256k1).to_pem()

    @staticmethod
    def 生成公钥(私钥):
        return ecdsa.SigningKey.from_pem(私钥).verifying_key.to_pem()

    @staticmethod
    def 生成地址(公钥):
        """
        [公钥]
        ↓ SHA-256哈希运算
        [中间哈希值]
        |      ↓RIPEMD-160哈希运算
        |      ↓连续两次SHA-256哈希运算
        ↓      ↓取前四位作为校验值
        [中间哈希值+校验值]
        ↓ Base58编码
        [地址]
        """

        公钥 = ecdsa.VerifyingKey.from_pem(公钥).to_string()

        中间哈希值 = hashlib.sha256(公钥).digest()

        # hashlib方式，适用于 print(hashlib.algorithms_available) 中含有ripemd160的系统
        # ripemd_160哈希 = hashlib.new('ripemd160')

        # pycryptodome方式
        ripemd_160哈希 = RIPEMD160.new()

        ripemd_160哈希.update(中间哈希值)
        ripemd_160哈希值 = ripemd_160哈希.digest()

        第二个哈希值 = hashlib.sha256(hashlib.sha256(ripemd_160哈希值).digest()).digest()

        校验值 = 第二个哈希值[:4]
        return base58.b58encode(ripemd_160哈希值 + 校验值)

    def 获取账户内容(self):
        return {'公钥': self.公钥.decode(), '私钥': self.私钥.decode(), '地址': self.地址.decode()}

    def 获取账户公钥(self):
        return self.公钥

    def 获取账户私钥(self):
        return self.私钥

    def 获取账户地址(self):
        return self.地址
