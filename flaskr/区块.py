import hashlib

from flaskr import 加密货币工具


class 区块类(object):
    def __init__(self, 父块哈希, 交易数据, 时间戳, 索引):
        self.父块哈希 = 父块哈希
        self.交易数据 = 交易数据
        self.时间戳 = 时间戳
        self.索引 = 索引
        self.默克尔根 = 加密货币工具.计算默克尔根(self.交易数据)
        self.区块哈希 = self.生成区块哈希()

    def 生成区块哈希(self):
        区块头 = str(self.父块哈希) + str(self.交易数据) + str(self.时间戳) + str(self.索引)
        return hashlib.sha256(区块头.encode()).hexdigest()

    def 获取区块内容(self):
        transaction_data = {}
        count = 0
        for i in self.交易数据:
            transaction_data[str(count)] = i.获取交易信息()
            count += 1
        print(transaction_data)
        # transaction_data = jsonify(transaction_data)
        return {'父块哈希': self.父块哈希, '交易数据': transaction_data, '时间戳': self.时间戳,
                '区块哈希': self.区块哈希,
                '索引': self.索引}

    def 获取区块哈希(self):
        return self.区块哈希

    def 获取区块索引(self):
        return self.索引

    def 获取区块交易数据(self):
        return self.交易数据

    def 获取区块默克尔根(self):
        return self.默克尔根
