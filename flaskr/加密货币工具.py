import hashlib

from flaskr.签名与验签工具 import 签名与验签工具


def 二进制化(数据):
    if not isinstance(数据, bytes):  # 如果数据编码不是二进制，则转换为二进制
        数据 = 数据.encode()
    return 数据


def 两次sha256(数据):
    数据 = 二进制化(数据)
    return hashlib.sha256(hashlib.sha256(数据).digest()).hexdigest()


def 计算默克尔根(数据):
    临时 = []
    for i in 数据:
        临时.append(str(i.获取交易哈希()))
    if len(临时) == 1:
        return 临时[0]
    while len(临时) > 1:
        if len(临时) % 2 == 1:
            临时.append(临时[-1])
        结果 = []
        for i in range(0, len(临时), 2):
            要添加的节点值 = ''.join(临时[i:i + 2])
            结果.append(hashlib.sha256(要添加的节点值.encode()).hexdigest())
        临时 = 结果
    return 临时[0]


def 验证交易(交易, 发送方的公钥):
    if '验签成功' == 签名与验签工具.验签(交易.获取交易哈希(), 交易.获取交易签名(), 发送方的公钥):
        return '验证成功'
    return '验证失败'


def 验证区块(区块):
    # 验证默克尔根是否正确
    计算得到的默克尔根 = 计算默克尔根(区块.获取区块交易数据())
    if 计算得到的默克尔根 == 区块.获取区块默克尔根():
        return '验证成功'
    return '验证失败'
