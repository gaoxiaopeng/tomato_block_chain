from datetime import datetime

from flaskr.交易 import 交易类
from flaskr.区块 import 区块类


class 区块链类(object):
    def __init__(self, 一把用于生成创世区块的私钥):
        self.区块链 = []  # 定义区块链数组
        self.__创建创世区块并添加到区块链上(一把用于生成创世区块的私钥)

    def __添加区块(self, 区块):
        self.区块链.append(区块)

    def 按哈希查询区块(self, 区块哈希):
        for i in self.区块链:
            if i.获取区块哈希() == 区块哈希:
                return i
        return "没找到区块"

    def 按索引查询区块(self, 索引):
        return self.区块链[索引].获取区块内容()

    def __创建创世区块并添加到区块链上(self, 私钥):
        # 第一笔交易,发送方和接收方地址都为'0'*32
        第一笔交易 = 交易类('0' * 32, '0' * 32, '第一笔交易', datetime.now(), 私钥)

        # 创世区块的的父块哈希为64个0
        创世区块 = 区块类('0' * 64, [第一笔交易], datetime.now(), 0)
        self.__添加区块(创世区块)

    def 添加区块(self, 交易数据):
        最后一个区块 = self.区块链[-1]
        区块 = 区块类(最后一个区块.获取区块哈希(), 交易数据, datetime.now(), 最后一个区块.获取区块索引() + 1)
        self.__添加区块(区块)
        return 最后一个区块.获取区块索引() + 1  # 返回新添加的区块的索引
