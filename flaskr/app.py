from datetime import datetime

from flask import Flask, request, jsonify
from flask_cors import CORS

from flaskr import 加密货币工具
from flaskr.交易 import 交易类
from flaskr.区块链 import 区块链类
from flaskr.区块链账户 import 区块链账户类
from flaskr.节点 import 节点类

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
CORS(app, supports_credentials=True)  # 跨域

一个用于生成创世区块的账户 = 区块链账户类()
区块链 = 区块链类(一个用于生成创世区块的账户.获取账户私钥())

# 定义相应消息
空内容 = {'响应代码': 404, '数据': '空'}
成功 = {'响应代码': 200, '数据': '好'}

# 创建模拟节点
节点1 = 节点类()


@app.route('/account_create', methods=['GET'])
def 创建账户():
    账户 = 区块链账户类()
    return jsonify({'响应代码': 200, '数据': 账户.获取账户内容()})


@app.route('/add_transaction', methods=['POST'])
def 添加交易():
    请求体 = request.json
    if '发送者' not in 请求体 or '接收者' not in 请求体 or '数据' not in 请求体 or '私钥' not in 请求体:
        return jsonify(空内容)
    新交易 = 交易类(请求体['发送者'], 请求体['接收者'], 请求体['数据'], datetime.now(), 请求体['私钥'])
    节点1.添加交易(新交易)
    return jsonify(成功)


@app.route('/add_block', methods=['GET'])
def 添加区块():
    索引 = 区块链.添加区块(节点1.交易池)
    节点1.清空交易池()
    return jsonify({'响应代码': 200, '数据': 索引})


@app.route('/query_block', methods=['GET'])
def 查询区块():
    索引 = int(request.args['索引'])
    return jsonify({'响应代码': 200, '数据': 区块链.按索引查询区块(索引)})


@app.route('/query_transaction', methods=['GET'])
def 查询交易():
    哈希 = request.args['哈希']
    for 区块 in 区块链.区块链:
        for 交易 in 区块.获取区块交易数据():
            if 哈希 == 交易.获取交易哈希():
                return jsonify({'响应代码': 200, '数据': 交易.获取交易信息()})
    return jsonify(空内容)


@app.route('/validate_transaction', methods=['POST'])
def 验证交易():
    请求体 = request.json
    if '哈希' not in 请求体 or '公钥' not in 请求体:
        return jsonify(空内容)
    哈希 = 请求体['哈希']
    for 区块 in 区块链.区块链:
        for 交易 in 区块.获取区块交易数据():
            if 哈希 == 交易.获取交易哈希():
                return jsonify({'响应代码': 200, '数据': 加密货币工具.验证交易(交易, 请求体['公钥'])})
    return jsonify(空内容)


@app.route('/validate_block', methods=['GET'])
def 验证区块():
    索引 = int(request.args['索引'])
    区块 = 区块链.区块链[索引]
    验证结果 = 加密货币工具.验证区块(区块)
    if 验证结果 == '验证成功':
        return jsonify({'响应代码': 200, '数据': '验证成功'})
    else:
        return jsonify({'响应代码': 500, '数据': '结果不符'})


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')  # host：IP地址  debug：是否debug
