import ecdsa


class 签名与验签工具(object):
    @staticmethod
    def 签名(数据, 私钥):
        if not isinstance(数据, bytes):  # 如果数据编码不是二进制，则转换为二进制
            数据 = 数据.encode()
        私钥 = ecdsa.SigningKey.from_pem(私钥)
        return 私钥.sign(数据)

    @staticmethod
    def 验签(数据, 签名, 公钥):
        if not isinstance(数据, bytes):  # 如果数据编码不是二进制，则转换为二进制
            数据 = 数据.encode()
        公钥 = ecdsa.VerifyingKey.from_pem(公钥)
        try:
            if 公钥.verify(签名, 数据):
                return '验签成功'
            else:
                return '验签失败'
        except Exception as e:
            print('\n\n 验签出现问题：\n\n')
            print(e)
            return '验签出现问题'
