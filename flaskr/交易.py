import binascii

from flaskr.加密货币工具 import 两次sha256
from flaskr.签名与验签工具 import 签名与验签工具


class 交易类(object):
    def __init__(self, 发送方的地址, 接收方的地址, 交易的内容, 交易的时间戳, 发送方的私钥):
        self.发送方的地址 = 发送方的地址
        self.接收方的地址 = 接收方的地址
        self.交易的内容 = 交易的内容
        self.交易的时间戳 = 交易的时间戳
        self.交易的哈希 = self.__生成交易的哈希()
        self.签名 = 签名与验签工具.签名(self.交易的哈希, 发送方的私钥)

    def __生成交易的哈希(self):
        交易元素字符串 = str(self.发送方的地址) + str(self.接收方的地址) + str(self.交易的内容) + str(self.交易的时间戳)
        return 两次sha256(交易元素字符串)

    def 获取交易信息(self):
        sign = binascii.hexlify(self.签名)  # 将byte解码为utf-8
        sign = sign.decode()
        return {'交易的哈希': self.交易的哈希, '发送方的地址': self.发送方的地址, ' 接收方的地址': self.接收方的地址,
                '交易的内容': self.交易的内容, '交易的时间戳': self.交易的时间戳,
                '签名': sign}

    def 获取交易哈希(self):
        return self.交易的哈希

    def 获取交易签名(self):
        return self.签名
